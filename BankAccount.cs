﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOONLV6
{
    class BankAccount
    {
        private string ownerName;
        private string ownerAdress;
        private decimal balance;

        public BankAccount(string ownerName, string ownerAdress, decimal balance)
        {
            this.ownerAdress = ownerAdress;
            this.ownerName = ownerName;
            this.balance = balance;
        }
        public void ChangeOwnerAdress(string adress)
        {
            this.ownerName = adress;
        }
        public void UpdateBalance(decimal amount) { this.balance += amount; }
        public string OwnerName { get { return this.ownerName; } }
        public string OwnerAdress { get { return this.ownerAdress; } }
        public decimal Balance { get { return this.balance; } }
    }
}
