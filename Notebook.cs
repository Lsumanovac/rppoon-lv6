﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOONLV6
{
    interface IAbstractCollection
    {
        IAbstractCollection GetIterator();
    }

    interface IAbstractIterator
    {
        Note First();
        Note Next();
        bool IsDone { get; }
        Note Current { get; }
    }
    class Notebook
    {
        private List<Note> notes;
        public Notebook()
        {
            this.notes = new List<Note>();
        }

        public Notebook(List<Note> notes)
        {
            this.notes = new List<Note>(notes.ToArray());
        }

        public void AddNote(Note note)
        {
            this.notes.Add(note);
        }
        public void RemoveNote(Note note)
        {
            this.notes.Remove(note);
        }
        public void Clear()
        {
            int i = 0;
            foreach(Note note in notes)
            {
                this.notes.RemoveAt(i);
            }
        } 
        public int Count { get { return this.notes.Count; } }
        public Note this[int index] { get { return this.notes[index]; } }
        public Iterator GetIterator() { return new Iterator(this); }
    }
}
