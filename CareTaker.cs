﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOONLV6
{
    class CareTaker
    {
        private int i;
        private List<Memento> previousStates;

        public CareTaker()
        {
            this.previousStates = new List<Memento>();
            i = previousStates.Count - 1;
        }
        public CareTaker(List<Memento> previousStates)
        {
            this.previousStates = previousStates;
            i = previousStates.Count - 1;
        }
        public Memento Undo()
        {
            --i;
            if (i < 0)
                return null;
            return previousStates[i];
        }
        public void AddPreviousState(Memento state) 
        { 
            previousStates.Add(state);
            i = previousStates.Count - 1;
        }
        public Memento Redo()
        {
            ++i;
            if (i > previousStates.Count - 1)
                return null;
            return previousStates[i];
        }
    }
}
