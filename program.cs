using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOONLV6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main1();
            //Main3();
            Main4();
        }

        static void Main1() //Main za 1. zadatak
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Title1", "Text1"));
            notebook.AddNote(new Note("LongerTitle2", "Text2"));
            notebook.AddNote(new Note("Title3", "LongerText3"));

            Iterator iterator = notebook.GetIterator();

            while (iterator.IsDone != true)
            {
                iterator.Current.show();
                iterator.Next();
            }

        }

        static void Main3() // Main za 3. zadatak
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem toDo = new ToDoItem("Title1", "Text1", new DateTime(2020, 5, 14));
            careTaker.AddPreviousState(toDo.StoreState());
            careTaker.AddPreviousState(toDo.StoreState());

            Console.WriteLine(toDo.ToString());
            toDo.RestoreState(careTaker.Undo());
            Console.WriteLine(toDo.ToString());
            toDo.RestoreState(careTaker.Redo());
            Console.WriteLine(toDo.ToString());
        }

        static void Main4()
        {
            BankAccount account = new BankAccount("Name1", "Adress1", 1500);
            BankMemento memento = new BankMemento(account);
            Console.WriteLine("Owner is: " + memento.OwnerName + ". \nOwner's adress is: " + memento.OwnerAdress
                + ".\nBalance: " + memento.Balance);
        }
    }
}
