﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOONLV6
{
    class BankMemento
    {
        public string OwnerName { get; private set; }
        public string OwnerAdress { get; private set; }
        public decimal Balance { get; private set; }
        public BankMemento(string ownerName, string ownerAdress, decimal balance)
        {
            this.OwnerAdress = ownerAdress;
            this.OwnerName = OwnerName;
            this.Balance = balance;
        }
        public BankMemento(BankAccount account)
        {
            this.OwnerName = account.OwnerName;
            this.OwnerAdress = account.OwnerAdress;
            this.Balance = account.Balance;
        }
        public void AddPreviousState(BankAccount account)
        {
            Balance = account.Balance;
            OwnerAdress = account.OwnerAdress;  
        }
    }
}
